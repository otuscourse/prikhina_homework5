﻿using Newtonsoft.Json;
using System.Diagnostics;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace ConsoleApp1.Classes
{
    [Serializable]
    public class F
    {
        public int i1;
        public int i2;
        public int i3;
        public int i4;
        public int i5;

        public F()
        {

        }

        public F(string i1, string i2, string i3, string i4, string i5)
        {
            this.i1 = Convert.ToInt32(i1);
            this.i2 = Convert.ToInt32(i2);
            this.i3 = Convert.ToInt32(i3);
            this.i4 = Convert.ToInt32(i4);
            this.i5 = Convert.ToInt32(i5);
        }

        public void Get()
        {
            i1 = 1; i2 = 2; i3 = 3; i4 = 4; i5 = 5;
        }

        #region Stopwatch
        public void GetStopWatchSerialize()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                SerializeDeserialize.Serialize(this);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции сериализации в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }

        public static void GetStopWatchDeserialize(Type targetType, string json)
        {
            Stopwatch stopwatch = new Stopwatch();
            //засекаем время начала операции
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                SerializeDeserialize.Deserialize(targetType, json);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции десериализации в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }

        public void Out()
        {
            var result = SerializeDeserialize.Serialize(this);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start(); for (int i = 0; i < 100; i++)
            {
                Debug.WriteLine($"Сериализованная строка: {result}");
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения вывода в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }
        #endregion

        #region Newtonsoft
        public F(string json)
        {
            var obj = JsonConvert.DeserializeObject<F>(json);
        i1 = obj.i1;
            i2 = obj.i2;
            i3 = obj.i3;
            i4 = obj.i4;
            i5 = obj.i5;
        }
        public String ToJson()
                => JsonConvert.SerializeObject(this);

        public void GetStopWatchNewtonsoftSerialize()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                this.ToJson();
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции сериализации Newtonsoft в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }

        public static void GetStopWatchNewtonsoftDeserialize(string json)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                new F(json);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции десериализации Newtonsoft в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }
        #endregion

        #region JsonSerializer
        public string GetJson()
        {
            var json = JsonSerializer.Serialize(this);
            return json;
        }

        public static F FromJson(string json)
        {
            var obj = JsonSerializer.Deserialize<F>(json);
            return obj;
        }

        public void GetStopWatchJsonSerializerSerialize()
        {
            var classF = new F();
            classF.Get();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                classF.GetJson();
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции сериализации JsonSerializer в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }

        public static void GetStopWatchJsonSerializerDeserialize(string json)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                F.FromJson(json);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции десериализации JsonSerializer в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }
        #endregion
    }
}
