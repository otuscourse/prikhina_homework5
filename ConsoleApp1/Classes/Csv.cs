﻿using System.Diagnostics;
using System.Text;

namespace ConsoleApp1.Classes
{
    public class Csv
    {
        public string fileName;
        public string fileData;
        public Csv(string fileName) 
        { 
            this.fileName = fileName;  
        }

        public void LoadData()
        {
            StringBuilder sb = new StringBuilder();

            using (StreamReader sr = new StreamReader(fileName))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }
            }

            fileData = sb.ToString();
        }

        public object[] Deserialize(Type targetType)
        {
            var data = fileData.Split("\r\n").Where(s => !String.IsNullOrEmpty(s)).ToList();
            var headers = data[0].Split(";");
            data.RemoveAt(0);
            object[] arr = new object[data.Count];
            var constructor = targetType.GetConstructors().Where(c => c.GetParameters().Length == headers.Length).FirstOrDefault();
            var costructorParams = constructor.GetParameters();

            for (int i = 0; i < data.Count; i++)
            {
                var values = data[i].Split(";");
                arr[i] = Activator.CreateInstance(targetType, values)!;
            }
            return arr;
        }

        public void GetStopWatch(Type targetType)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                Deserialize(targetType);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время выполнения операции десериализации csv в цикле на 100 итераций: {stopwatch.ElapsedMilliseconds}мс");
        }
    }
}
