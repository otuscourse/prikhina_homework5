﻿using System.Text;
using System.Text.Json.Nodes;

namespace ConsoleApp1.Classes
{
    public class SerializeDeserialize
    {
        public static string Serialize(object obj)
        {
            var result = new StringBuilder();
            result.Append("{");
            var fields = obj.GetType().GetFields();
            foreach (var field in fields)
            {
                var fieldType = field.FieldType.GetType();
                var value = field.GetValue(obj);
                result.AppendFormat("\"{0}\":{1}", field.Name, fieldType == typeof(string) ? $"\"{value}\"" : value);
                if (field != fields.Last())
                    result.Append(",");
            }
            result.Append("}");

            return result.ToString();
        }

        public static object Deserialize(Type targetType, string jsonString)
        {
            var jsNode = JsonObject.Parse(jsonString);
            var constructor = targetType.GetConstructors().Where(c => c.GetParameters().Length == jsNode.AsObject().Count).FirstOrDefault();
            var costructorParams = constructor.GetParameters();
            object[] constructorValues = new object[costructorParams.Length];
            for (int i = 0; i < constructorValues.Length; i++)
            {
                var param = costructorParams[i];
                constructorValues[i] = Convert.ToString(jsNode[param.Name!]);
            }
            return Activator.CreateInstance(targetType, constructorValues)!;
        }

        
    }
}
