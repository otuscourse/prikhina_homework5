﻿using ConsoleApp1.Classes;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var classF = new F();
            classF.Get();
            Console.WriteLine("Мой рефлекшн:");
            classF.GetStopWatchSerialize();
            Console.WriteLine($"Сериализованная строка: {SerializeDeserialize.Serialize(classF)}");
            classF.Out();
            F.GetStopWatchDeserialize(typeof(F), SerializeDeserialize.Serialize(classF));
            var exampleData = "{\"i1\":5,\"i2\":4,\"i3\":3,\"i4\":1,\"i5\":1}";

            Console.WriteLine("\nСтандартный механизм (NewtonsoftJson):");
            F.GetStopWatchNewtonsoftDeserialize(exampleData);
            classF.GetStopWatchNewtonsoftSerialize();

            Console.WriteLine("\nСтандартный механизм (JsonSerializer):");
            F.GetStopWatchJsonSerializerDeserialize(exampleData);
            classF.GetStopWatchNewtonsoftSerialize();

            Console.WriteLine("\nРабота с файлом:");
            var csv = new Csv(@"C:\folder\file.csv");
            csv.LoadData();
            csv.GetStopWatch(typeof(F));
            var objects = csv.Deserialize(typeof(F));
            Console.ReadLine();
        }
    }
}